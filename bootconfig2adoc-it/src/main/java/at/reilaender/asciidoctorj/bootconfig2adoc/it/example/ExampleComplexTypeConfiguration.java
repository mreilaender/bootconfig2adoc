/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.it.example;

import java.util.List;
import javax.validation.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * It is important that the fields have getter and setter.
 * <p>
 * Some highlevel parameters.
 */
@ConfigurationProperties("complex-type-config")
public class ExampleComplexTypeConfiguration {

    /**
     * The Server url. For example: htto://www.example.com
     */
    @NotBlank
    private List<NestedConfig> properties;

    public List<NestedConfig> getProperties() {
        return properties;
    }

    public void setProperties(List<NestedConfig> properties) {
        this.properties = properties;
    }

    static class NestedConfig {

        private String someProperty;

        public String getSomeProperty() {
            return someProperty;
        }

        public void setSomeProperty(String someProperty) {
            this.someProperty = someProperty;
        }
    }

}
