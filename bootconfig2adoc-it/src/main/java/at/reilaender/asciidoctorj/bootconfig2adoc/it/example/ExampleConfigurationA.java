/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.it.example;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * It is important that the fields have getter and setter.
 */
@ConfigurationProperties("configA")
public class ExampleConfigurationA {

	/**
	 * This is some privite int paramter.
	 */
	private int someInt;

	/**
	 * This is an wrapper Integer type paramter with NotNull Contraint.
	 */
	@NotNull
	private Integer anInteger;

	/**
	 * This is an String.
	 * And this is some {@code code xample}
	 * <ul>
	 * 	<li>This is some Html list</li>
	 * </ul>
	 */
	@NotBlank
	@NotNull
	private String someString;

	public int getSomeInt() {
		return someInt;
	}

	public void setSomeInt(int someInt) {
		this.someInt = someInt;
	}

	public Integer getAnInteger() {
		return anInteger;
	}

	public void setAnInteger(Integer anInteger) {
		this.anInteger = anInteger;
	}

	public String getSomeString() {
		return someString;
	}

	public void setSomeString(String someString) {
		this.someString = someString;
	}



}
