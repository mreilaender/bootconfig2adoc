/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import at.reilaender.asciidoctorj.bootconfig2adoc.testutil.AsciidoctorAssert;
import at.reilaender.asciidoctorj.bootconfig2adoc.testutil.FileLoaderUtil;

public class DocumentTest {

	private static Path HTML_DOC_FILE = FileLoaderUtil.getMavenTargetDirectory()
			.resolve(Paths.get("site", "asciidoc", "html", "demo.html"));

	@Nested
	public class HtmlDocumentTest {
		@Test
		public void testDocIsGenerated() {
			assertThat(HTML_DOC_FILE).exists();
		}

		@Test
		public void testContainsTable() {
			assumeThat(HTML_DOC_FILE).exists();

			String content = FileLoaderUtil.readFile(HTML_DOC_FILE);
			assertThat(content).satisfies(AsciidoctorAssert.assertContainsConfigTable());
		}

		@ValueSource(strings = { "configA.an-integer", "configA.some-int", "configA.some-string", "configB.server-url" })
		@ParameterizedTest
		public void testContainsTableProperties(String propertyName) {
			assumeThat(HTML_DOC_FILE).exists();
			String content = FileLoaderUtil.readFile(HTML_DOC_FILE);
			assumeThat(content).satisfies(AsciidoctorAssert.assertContainsConfigTable());

			assertThat(content).satisfies(AsciidoctorAssert.assertContainsConfigProperty(propertyName));
		}
	}

	private static Path PDF_DOC_FILE = FileLoaderUtil.getMavenTargetDirectory()
			.resolve(Paths.get("site", "asciidoc", "pdf", "demo.pdf"));

	@Nested
	public class PdfDocumentTest {

		@Test
		public void testDocIsGenerated() {
			assertThat(PDF_DOC_FILE).exists();
		}

	}

}
