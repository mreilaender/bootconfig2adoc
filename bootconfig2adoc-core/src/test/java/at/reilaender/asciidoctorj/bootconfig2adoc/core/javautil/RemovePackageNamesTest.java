package at.reilaender.asciidoctorj.bootconfig2adoc.core.javautil;

import static org.assertj.core.api.Assertions.assertThat;

import at.reilaender.asciidoctorj.bootconfig2adoc.core.JavaUtils;
import org.junit.jupiter.api.Test;

public class RemovePackageNamesTest {

    @Test
    void withGenericType_shouldRemovePackageNameFromGenericType() {
        final String nameWithPackages = "java.util.List<com.test.SomeClass>";

        assertThat(JavaUtils.removePackageNames(nameWithPackages))
            .doesNotContain("java.util");
    }

    @Test
    void withGenericType_shouldRemovePackageNameFromType() {
        final String nameWithPackages = "java.util.List<com.test.SomeClass>";

        assertThat(JavaUtils.removePackageNames(nameWithPackages))
            .doesNotContain("com.test");
    }

    @Test
    void withGenericType_shouldRemovePackageNameFromBothTypes() {
        final String nameWithPackages = "java.util.List<com.test.SomeClass>";

        assertThat(JavaUtils.removePackageNames(nameWithPackages))
            .isEqualTo("List<SomeClass>");
    }

    @Test
    void withNormalType_shouldIncludeClassName() {
        final String nameWithPackages = "com.test.SomeClass";

        assertThat(JavaUtils.removePackageNames(nameWithPackages))
            .contains("SomeClass");
    }

    @Test
    void withNormalType_shouldNotIncludePackage() {
        final String nameWithPackages = "com.test.SomeClass";

        assertThat(JavaUtils.removePackageNames(nameWithPackages))
            .doesNotContain("com.test");
    }
}
