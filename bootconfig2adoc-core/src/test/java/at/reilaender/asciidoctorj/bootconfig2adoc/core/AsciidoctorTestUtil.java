/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import java.util.Objects;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.OptionsBuilder;
import org.asciidoctor.SafeMode;


/**
 *
 * BTW.: This class depends on BootConfig2adocBlockMacroProcessorExtension, therefore it can not been moved to test-util module
 */
public class AsciidoctorTestUtil {

	/**
	 * Convert the given adoc document to an HTML document.
	 * @param adocContent the AsciiDoc content
	 * @return the HTML content
	 */
	public static String convert(final String adocContent) {
		Objects.requireNonNull(adocContent, "argument `adocContent` must not be null");

		return AsciidoctorTestUtil.getAsciidoctocWithExtension().convert(adocContent,
				AsciidoctorTestUtil.getOptionBuilder());
	}

	/**
	 * Create a new Asciidoctor instance with an (explicit) registered {@link SpringBootConfigExtension}.
	 * @return a new instance of Asciidoctor
	 */
	public static Asciidoctor getAsciidoctocWithExtension() {
		Asciidoctor asciidoctor = Asciidoctor.Factory.create();
		new SpringBootConfigExtension().register(asciidoctor);
		return asciidoctor;
	}

	public static OptionsBuilder getOptionBuilder() {
		return OptionsBuilder.options().headerFooter(true).safe(SafeMode.SERVER);
	}

}
