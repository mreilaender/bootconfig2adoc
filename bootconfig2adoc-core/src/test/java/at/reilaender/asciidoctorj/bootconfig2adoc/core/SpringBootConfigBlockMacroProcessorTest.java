/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import at.reilaender.asciidoctorj.bootconfig2adoc.testutil.AsciidoctorAssert;
import at.reilaender.asciidoctorj.bootconfig2adoc.testutil.FileLoaderUtil;

public class SpringBootConfigBlockMacroProcessorTest {

	@Nested
	public class LoadConfigTest {

		@Test
		void testAbsolutePath() {
			String adoc = demoAdoc(getConfigFilePath().toAbsolutePath());
			String htmlResult = AsciidoctorTestUtil.convert(adoc);

			assertThat(htmlResult).satisfies(AsciidoctorAssert.assertContainsConfigTable());
		}

		@Test
		void testRelativePath() {
			Path currentPath = Paths.get("").toAbsolutePath();
			Path relativPath = currentPath.relativize(getConfigFilePath().toAbsolutePath());

			String adoc = demoAdoc(relativPath);
			String htmlResult = AsciidoctorTestUtil.convert(adoc);

			assertThat(htmlResult).satisfies(AsciidoctorAssert.assertContainsConfigTable());
		}
	}

	private static Path getConfigFilePath() {
		return FileLoaderUtil.getResourceAsFile("spring-configuration-metadata.json");
	}

	private static String demoAdoc(Path path) {
		return "= BootConfig2adoc\n" + "\n" + "springBootConfig::" + path.toString() + "[]\n";
	}

}
