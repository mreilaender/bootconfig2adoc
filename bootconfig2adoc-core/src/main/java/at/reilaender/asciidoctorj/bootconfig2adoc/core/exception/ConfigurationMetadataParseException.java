package at.reilaender.asciidoctorj.bootconfig2adoc.core.exception;

public class ConfigurationMetadataParseException extends ConfigurationMetadataReadException {

    public ConfigurationMetadataParseException(Throwable cause) {
        super("Could not parse configuration metadata file", cause);
    }
}
