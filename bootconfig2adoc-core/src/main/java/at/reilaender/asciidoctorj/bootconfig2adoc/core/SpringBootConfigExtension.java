/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import java.util.Objects;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.extension.JavaExtensionRegistry;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * Register {@link SpringBootConfigBlockMacroProcessor} in AsciidoctorJ Extension Registry.
 *
 * This class needs to been registered via Java Service Provider Interface
 * (META-INF/services/org.asciidoctor.extension.spi.ExtensionRegistry file).
 *
 * @author engelmann
 */
public class SpringBootConfigExtension implements ExtensionRegistry {

	@Override
	public void register(final Asciidoctor asciidoctor) {
		Objects.requireNonNull(asciidoctor, "argument `asciidoctor` must not be null");

		JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();
		extensionRegistry.blockMacro("springBootConfig", SpringBootConfigBlockMacroProcessor.class);
	}
}
