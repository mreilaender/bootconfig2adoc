package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import at.reilaender.asciidoctorj.bootconfig2adoc.core.exception.ConfigurationMetadataReadException;
import org.springframework.boot.configurationprocessor.metadata.ConfigurationMetadata;

public interface ConfigurationMetadataReader {

    ConfigurationMetadata read() throws ConfigurationMetadataReadException;
}
