/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import at.reilaender.asciidoctorj.bootconfig2adoc.core.exception.ConfigurationMetadataParseException;
import at.reilaender.asciidoctorj.bootconfig2adoc.core.exception.ConfigurationMetadataReadException;
import java.io.InputStream;
import java.util.Objects;
import org.springframework.boot.configurationprocessor.metadata.ConfigurationMetadata;
import org.springframework.boot.configurationprocessor.metadata.JsonMarshaller;

public class ConfigurationMetadataReaderInputStreamImpl implements ConfigurationMetadataReader {

    private final InputStream inputStream;

    public ConfigurationMetadataReaderInputStreamImpl(final InputStream inputStream) {
        Objects.requireNonNull(inputStream, "inputStream must not be null");

        this.inputStream = inputStream;
    }

    @Override
    public ConfigurationMetadata read() throws ConfigurationMetadataReadException {
        try {
            return new JsonMarshaller().read(inputStream);
        } catch (Exception e) {
            throw new ConfigurationMetadataParseException(e);
        }
    }

}
