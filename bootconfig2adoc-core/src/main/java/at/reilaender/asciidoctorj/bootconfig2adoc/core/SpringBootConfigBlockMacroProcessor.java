/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import at.reilaender.asciidoctorj.bootconfig2adoc.core.exception.ConfigurationMetadataReadException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import org.asciidoctor.ast.Column;
import org.asciidoctor.ast.Row;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.ast.Table;
import org.asciidoctor.extension.BlockMacroProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.metadata.ConfigurationMetadata;
import org.springframework.boot.configurationprocessor.metadata.ItemMetadata;
import org.springframework.boot.configurationprocessor.metadata.ItemMetadata.ItemType;

public class SpringBootConfigBlockMacroProcessor extends BlockMacroProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootConfigBlockMacroProcessor.class);

    private final String PACKAGE_NAME_SEPARATOR = ".";

	public SpringBootConfigBlockMacroProcessor(String name, Map<String, Object> config) {
		super(name, config);
	}

	@Override
	public Object process(StructuralNode parent, String target, Map<String, Object> attributes) {
		Objects.requireNonNull(parent, "argument `parent` must not be null");
		Objects.requireNonNull(target, "argument `target` must not be null");
		Objects.requireNonNull(attributes, "argument `attributes` must not be null");

		LOGGER.debug("process: target=`{}` attributes={}", target, attributes);
		try (InputStream inputStream = Files.newInputStream(Paths.get(target))) {

			boolean printTypePackage = Boolean
					.parseBoolean(attributes.getOrDefault("printTypePackage", "false").toString());

			ConfigurationMetadataReader configurationMetadataReader =
                new ConfigurationMetadataReaderInputStreamImpl(inputStream);
			ConfigurationMetadata configurationMetadata = configurationMetadataReader.read();
			LOGGER.debug("configurationMetadata: `{}`", configurationMetadata);

			return createTable(configurationMetadata, parent, printTypePackage);
		} catch (IOException | RuntimeException | ConfigurationMetadataReadException e) {
			LOGGER.error("errror while processing target=`{}`", target, e);
			return createBlock(parent, "paragraph", e.toString());
        }
    }

	public Table createTable(ConfigurationMetadata configurationMetadata, StructuralNode parent,
			boolean printTypePackage) {
		Objects.requireNonNull(configurationMetadata, "argument `configurationMetadata` must not be null");
		Objects.requireNonNull(parent, "argument `parent` must not be null");

		final Table table = createTable(parent);
		table.setGrid("rows");
		table.setTitle("Configuration Properties");

		class ColumnConstructionHelper {
			private Column createMyColumn(int index, Table.HorizontalAlignment horizontalAlignment, String style) {
				final Column column = createTableColumn(table, index);
				column.setHorizontalAlignment(horizontalAlignment);
				column.setVerticalAlignment(Table.VerticalAlignment.TOP);
				column.setStyle(style);
				return column;
			}
		}
		ColumnConstructionHelper columnHelper = new ColumnConstructionHelper();

		final Column attributeNameColumn = columnHelper.createMyColumn(0, Table.HorizontalAlignment.LEFT, "literal");
		final Column attributeTypeColumn = columnHelper.createMyColumn(1, Table.HorizontalAlignment.LEFT, "monospaced");
		final Column attributeDocuColumn = columnHelper.createMyColumn(2, Table.HorizontalAlignment.LEFT, "verse");

		class RowConstructionHelper {
			private Row createMyRow(String name, String type, String docu) {
				final Row row = createTableRow(table);
				row.getCells().add(createTableCell(attributeNameColumn, name));
				row.getCells().add(createTableCell(attributeTypeColumn, type));
				row.getCells().add(createTableCell(attributeDocuColumn, docu));
				return row;
			}

			private void addBodyRow(ItemMetadata item) {
				table.getBody()
						.add(createMyRow(item.getName(),
								printTypePackage ? item.getType()
										: JavaUtils.removePackageNames(item.getType()),
								item.getDescription()));
			}
		}
		final RowConstructionHelper rowHelper = new RowConstructionHelper();

		table.getHeader().add(rowHelper.createMyRow("property name", "type", "documentation"));
		configurationMetadata.getItems().stream().filter(item -> item.isOfItemType(ItemType.PROPERTY))
				.forEach(rowHelper::addBodyRow);

		return table;
	}

}
