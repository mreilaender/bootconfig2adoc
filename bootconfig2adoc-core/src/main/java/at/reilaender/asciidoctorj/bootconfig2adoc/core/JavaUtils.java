package at.reilaender.asciidoctorj.bootconfig2adoc.core;

import org.apache.commons.lang3.StringUtils;

public class JavaUtils {

    public static final String PACKAGE_NAME_SEPARATOR = ".";

    public static String removePackageNames(final String input) {
        if (isGenericType(input)) {
            String genericType = extractGenericType(input);
            String baseType = extractBaseType(input);

            return removePackageName(baseType) + "<" + removePackageName(genericType) + ">";
        }

        return removePackageName(input);
    }

    private static boolean isGenericType(final String input) {
        return input.contains("<") && input.contains(">");
    }

    private static String extractGenericType(final String input) {
        return input.split("<")[1].split(">")[0];
    }

    private static String extractBaseType(final String input) {
        return input.split("<")[0];
    }

    private static String removePackageName(final String input) {
        return StringUtils.substringAfterLast(input, PACKAGE_NAME_SEPARATOR);
    }
}
