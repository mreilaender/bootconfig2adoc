package at.reilaender.asciidoctorj.bootconfig2adoc.core.exception;

public abstract class ConfigurationMetadataReadException extends Exception {

    public ConfigurationMetadataReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
