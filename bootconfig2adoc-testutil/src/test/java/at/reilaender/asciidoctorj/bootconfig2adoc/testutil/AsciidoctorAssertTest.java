package at.reilaender.asciidoctorj.bootconfig2adoc.testutil;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class AsciidoctorAssertTest {

	@Nested
	public class AssertContainsConfigTableTest {

		@Test
		public void testMatch() {
			// @formatter:off
			String content = ""
			        + "<h1>BootConfig2adoc</h1>\n"
					+ "</div>\n" + "<div id=\"content\">\n"
					+ "<table class=\"tableblock frame-all grid-rows stretch\">\n"
					+ "<caption class=\"title\">Configuration Properties</caption>\n"
					+ "<colgroup>\n"
					+ "</colgroup>\n"
					+ "<thead>\n";
			// @formatter:on

			assertThat(AsciidoctorAssert.assertContainsConfigTable().matches(content)).isEqualTo(true);
		}

		@Test
		public void testNoTable() {
			// @formatter:off
			final String content = "Some text";
			// @formatter:on

			assertThat(AsciidoctorAssert.assertContainsConfigTable().matches(content)).isEqualTo(false);
		}

		@Test
		public void testOtherTable() {
			// @formatter:off
			final String content = ""
					+ "<table class=\"tableblock frame-all grid-rows stretch\">\n"
					+ "<caption class=\"title\">Other Table</caption>\n";
			// @formatter:on

			assertThat(AsciidoctorAssert.assertContainsConfigTable().matches(content)).isEqualTo(false);
		}
	}

}
