/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.testutil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;

public class FileLoaderUtil {

	public static Path getMavenTargetDirectory() {
		try {
			URL resourceUri = Thread.currentThread().getContextClassLoader().getResource("");
			return Paths.get(resourceUri.toURI()).getParent();
		} catch (URISyntaxException e) {
			throw new RuntimeException("error while get current director", e);
		}
	}

	/**
	 * Finds the resource with the given name.
	 *
	 * <p>
	 * The name of a resource is a '{@code /}'-separated path name that identifies
	 * the resource.
	 * </p>
	 *
	 * @param resourceName the resource name
	 * @return the resource file; or empty if the resource was not found
	 */
	public static Optional<Path> findResourceAsFile(final String resourceName) {
		Objects.requireNonNull(resourceName, "argument `resourceName` must not be null");

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resourceUri = classLoader.getResource(resourceName);

		if (resourceUri != null) {
			try {
				return Optional.of(Paths.get(resourceUri.toURI()));
			} catch (URISyntaxException e) {
				throw new RuntimeException("error while get file for `" + resourceName + "` ", e);
			}
		} else {
			return Optional.empty();
		}
	}

	public static Path getResourceAsFile(final String resourceName) {
		Objects.requireNonNull(resourceName, "argument `resourceName` must not be null");

		return findResourceAsFile(resourceName)
				.orElseThrow(() -> new RuntimeException("resource `" + resourceName + "` not found"));
	}

	public static String readResource(final String resourceName) {
		Objects.requireNonNull(resourceName, "argument `resourceName` must not be null");

		try {
			return new String(Files.readAllBytes(getResourceAsFile(resourceName)), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException("error while reading resource: `" + resourceName + "`", e);
		}
	}

	public static String readFile(final Path file) {
		Objects.requireNonNull(file, "argument `file` must not be null");

		try {
			return new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException("error while file resource: `" + file + "`", e);
		}
	}
}
