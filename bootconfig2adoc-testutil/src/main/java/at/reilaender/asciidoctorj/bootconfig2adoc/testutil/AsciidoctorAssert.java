/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.reilaender.asciidoctorj.bootconfig2adoc.testutil;

import java.util.Objects;
import java.util.regex.Pattern;

import org.assertj.core.api.Condition;

/**
 * AssertJ {@link Condition} to check the html output.
 * @author engelmann
 */
public class AsciidoctorAssert {

	/** Util classes need no constructor. */
	private AsciidoctorAssert() {
		super();
	}

	/**
	 * Provides an {@link Condition} to assert that the value under test (String with HTML content) contains
	 * an HTML properties table.
	 *
	 * <p>
	 * Usage:
	 * {@code assertThat(htmlContent).satisfies(AsciidoctorAssert.assertContainsConfigTable());}
	 * </p>
	 *
	 * @return the condition
	 */
	public static Condition<String> assertContainsConfigTable() {
		return new Condition<String>(AsciidoctorAssert::containsConfigTable, "contains properties table");
	}

	private static final Pattern CONFIG_TABLE_PATTERN = Pattern
			.compile("<table[^<]*<caption class=\"title\">Configuration Properties</caption>", Pattern.DOTALL);

	private static boolean containsConfigTable(String html) {
		return CONFIG_TABLE_PATTERN.matcher(html).find();
	}

	/**
	 * Provides an {@link Condition} to assert that the value under test (String  with HTML content) contains the
	 * property name.
	 *
	 * <p>
	 * Usage:
	 * {@code assertThat(htmlContent).satisfies(AsciidoctorAssert.assertContainsConfigProperty("firstName"));}
	 * </p>
	 *
	 * @param propertyName the name of the expected property
	 * @return the condition
	 */
	public static Condition<String> assertContainsConfigProperty(final String propertyName) {
		Objects.requireNonNull(propertyName, "argument `propertyName` must not be null");

		return new Condition<String>((html) -> containsConfigProperty(html, propertyName),
				"contain parameter `" + propertyName + "`");
	}

	private static boolean containsConfigProperty(final String html, final String propertyName) {
		return html.contains(propertyName);
	}
}
