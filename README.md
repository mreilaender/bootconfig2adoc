# BootConfig2adoc

```
    ____                  __    ______                   ____    _             ___                __              
   / __ )  ____   ____   / /_  / ____/  ____    ____    / __/   (_)   ____ _  |__ \  ____ _  ____/ /  ____   _____
  / __  | / __ \ / __ \ / __/ / /      / __ \  / __ \  / /_    / /   / __ `/  __/ / / __ `/ / __  /  / __ \ / ___/
 / /_/ / / /_/ // /_/ // /_  / /___   / /_/ / / / / / / __/   / /   / /_/ /  / __/ / /_/ / / /_/ /  / /_/ // /__  
/_____/  \____/ \____/ \__/  \____/   \____/ /_/ /_/ /_/     /_/    \__, /  /____/ \__,_/  \__,_/   \____/ \___/  
                                                                   /____/                                         
```


Spring-Boot-Configuration to AsciiDoc: AsciiDoctorJ-Extension to embed Spring-Boot configuration metadata files in AsciiDoc documents.

Maven
-----
Available from [Maven Central](https://search.maven.org/search?q=g:at.reilaender.asciidoctorj.bootconfig2adoc)

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.humanfork.asciidoctorj.bootconfig2adoc/bootconfig2adoc-adoc/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.humanfork.asciidoctorj.bootconfig2adoc/bootconfig2adoc-adoc)

```
<dependency>
    <groupId>at.reilaender.asciidoctorj.bootconfig2adoc</groupId>
    <artifactId>bootconfig2adoc-adoc</artifactId>
    <version>0.1.1</version> <!-- or newer -->
</dependency>
```


Website
-------
- https://www.humanfork.de/projects/bootconfig2adoc/ (for releases)
- https://humanfork.gitlab.io/bootconfig2adoc/index.html (for snapshots)

Example:
- [demo.html](https://www.humanfork.de/projects/bootconfig2adoc/bootconfig2adoc-it/asciidoc/html/demo.html)
- [demo.pdf](https://www.humanfork.de/projects/bootconfig2adoc/bootconfig2adoc-it/asciidoc/pdf/demo.pdf)
