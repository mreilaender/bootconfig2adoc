echo build the project documentation

echo switching directory to project root
%~d0
cd %~p0

call mvn org.asciidoctor:asciidoctor-maven-plugin:process-asciidoc@doc-adoc2pdf --non-recursive

start target/site/doc/asciidoc/pdf/user-guide.pdf